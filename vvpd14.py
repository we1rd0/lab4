def input_check(N): # Проверка ввода
    if type(N) is int and N >= 5:
        return True
    else:
        return False

def task_solution(N): # Решение задачи
    loot = [x for x in range(1, N) if (x*3 != N) and (N - x > x) and (N - x) % 2 == 0] #Это можно было решить в одну строку
    print("Количество разбиений отрезка для получения равнобедренного треугольника: " + str(len(loot)))


def main(): # Точка входа
    while True:
            try:
                N = int(input("Введите длину отрезка: "))
                if (N < 5):
                    print("Impossible.")
                    continue
            except ValueError:
                print("Длина отрезка введена некорректно.")
            else:
                break
    input_check(N)
    task_solution(N)
if __name__ == "__main__":
    main()