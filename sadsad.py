def main():
    while True:
        try:
            N = int(input("Введите длину отрезка: "))
            if (N < 6) or (N % 2 == 1):
                print("Impossible.")
                continue
        except ValueError:
            print("Длина отрезка введена некорректно.")
        else:
            break
    N //= 2
    loot = [(N - x) for x in range(1, N) if (N - x) != (N / 2)] #Это можно было решить в одну строку
    print(len(loot)//2) 
if __name__ == "__main__":
    main()