def input_check(N):
    if type(N) is int and N >= 5:
        return True
    else:
        return False

def task_solution(N):
    loot = [x for x in range(1, N) if (x*3 != N) and (N - x > x) and (N - x) % 2 == 0] #Это можно было решить в одну строку
    return len(loot)

def test_one():
    assert input_check("Пять") == False

def test_two():
    assert input_check('5') == False

def test_three():
    assert input_check(5) == True

def test_task_solution_one():
    assert task_solution(4) == 0
    
def test_task_solution_two():
    assert task_solution(7) == 2